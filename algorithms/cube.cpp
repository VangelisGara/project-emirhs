#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include "../headers/HashTable.h"
#include "../headers/Insights.h"
#include "../headers/MathHelper.h"
#include "../headers/VectorSpace.h"

int memory_usage;

int main(int argc, char *argv[]){
  auto start_time = chrono::high_resolution_clock::now();
  memory_usage = 0; // start counting the memory

  vector<string> Arguments(argv, argv + argc); // store all argument into a string vector

  // basic arguments
  string input_file, query_file, output_file;
  int k=3 , M=10 , probes=10;
  string metric = "euclidean";
  double Radius=0;
  double appraoch_fraction,max_approach_fraction = -1;
  double m_tCUBE=0;

  // process all arguments
  for( int i = 0; i < argc; i++){
    if( Arguments[i] == "-d")
      input_file = Arguments[i+1];
    if( Arguments[i] == "-q")
      query_file = Arguments[i+1];
    if( Arguments[i] == "-o")
      output_file = Arguments[i+1];
    if( Arguments[i] == "-k")
      k = stoi(Arguments[i+1]);
    if( Arguments[i] == "-M")
      M = stoi(Arguments[i+1]);
    if( Arguments[i] == "-probes")
      probes = stoi(Arguments[i+1]);
    if( Arguments[i] == "-R")
      Radius = stod(Arguments[i+1]);
    if( Arguments[i] == "-m")
      metric = (Arguments[i+1]);
  }

  // size of arguments
  memory_usage += sizeof(vector<string>) + (sizeof(string) * Arguments.size());

  // output file
  ofstream out;
  out.open(output_file,ios::app);

  // program info
  cout << endl << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ " << endl;
  cout << " VECTOR SPACE WILL BE BASED ON DATASET: " << input_file << endl;
  cout << " RUN THE QUERIES FROM: " << query_file << endl;
  cout << " THE RESULTS WILL BE STORED TO: " << output_file << endl;
  cout << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ " << endl;
  cout << " Algorithm used: HYBERCUBE" << endl;
  cout << " Metric: " << metric << endl;
  cout << " #probes: " << probes << "   #k: " << k  << "   #M: " << M << endl;
  cout << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ " << endl;

  // program info on output file
  time_t now = time(0);
  tm *ltm = localtime(&now);
  out << "------------------------------------------------------------------" << endl;
  out << "Algorithm: Hypercube " << "Metric: " << metric << " Radius: " << Radius << endl;
  out << "Using input file: " << input_file << endl;
  out << "#probes: " << probes << " #k: " << k << " #M: " << M << endl;
  out << ltm->tm_mday << "-" << 1 + ltm->tm_mon << "-" << 1900 + ltm->tm_year << " ";
  out << ltm->tm_hour << ":" << ltm->tm_min << ":" << 1 + ltm->tm_sec << endl;
  out << "------------------------------------------------------------------" << endl;

  cout << "Creating the vector space based on input file..." << endl;

  VectorSpace D(input_file);  // create a vector space

  int d2 = floor(log2(D.getNumberOfPoints()));// k will be n for hybercube , in order to compute d'

  vector<int> r; // r family same for each hash table
  for(int i=0; i<k ; i++)
    r.push_back((rand() % 10)); // create the r family

  HashTable L(d2,r,D.getDimension()); // create a hash table
  L.HashVectorSpace(&D,D.getNumberOfPoints(),metric,"cube"); // fill it after hashing a vector space
  //L.printHashTable();
  //int temp;cin >> temp;

  string answer; // for multiple query files
  do{
    cout << "Processing the query file" << endl;
    VectorSpace Q(query_file); // Will hold all query points

    // for every point in query file
    int queries = Q.getNumberOfPoints();
    vector<int>* idsQ = Q.getIds();
    vector<int>* idsD = D.getIds();
    for(int i=0; i< queries ; i++){
      cout << endl << "Query: " << (*idsQ)[i] << endl;out << endl << "Query: " << (*idsQ)[i] << endl;

      double distanceCUBE = 1000000;

      auto start_timeCUBE = chrono::high_resolution_clock::now();

      // R-nearest neighbors calculation
      if( Radius > 0){ // only approx neighbor will be calculated
        cout << "R-Near neighbors: " << endl;out << "R-Near neighbors: " << endl;
      }
      vector<int> Result,results;
      // for each table
      Result = L.RangeSearchCube(Radius,&D,&(Q.Dspace[i]),M,probes,D.getNumberOfPoints(),metric); // nearest neighbors

      // Print the ids of the nearest neighbors
      int numberNearestNeighbors = Result.size();
      if( numberNearestNeighbors == 0){
        if(Radius > 0){
          cout << "None" << endl; out << "None" << endl;
        }
      }
      else{
        // remove neighbors that are in list many times
        sort( Result.begin(), Result.end() );
        Result.erase(unique( Result.begin(), Result.end() ), Result.end() );
        // print nearest neighbors
        if( Radius > 0){
          for(uint p1 = 0 ; p1 < Result.size() ; p1++ ){
            cout << (*idsD)[Result[p1]] << endl;out << (*idsD)[Result[p1]] << endl;
          }
        }
      }

      int b = -1;
      // Approximate nearest neighbor
      if( numberNearestNeighbors > 0){
        cout << "Nearest Neighbor: ";out << "Nearest Neighbor: ";
        for(uint p1=0; p1<Result.size(); p1++){
          if (metric == "euclidean"){
            double dist = euclidean_distance( &(D.Dspace[Result[p1]]) , &(Q.Dspace[i]) );
            if( dist < distanceCUBE){
              distanceCUBE = dist;
              b = Result[p1];
            }
          }
          else if(metric == "cosine"){
            double dist = cosine_distance( &(D.Dspace[Result[p1]]) , &(Q.Dspace[i]) );
            if( dist < distanceCUBE){
              distanceCUBE = dist;
              b = Result[p1];
            }
          }
        }
        cout << (*idsD)[b] << endl;out << (*idsD)[b] << endl;
        cout << "distanceCUBE: " << distanceCUBE << endl;out << "distanceCUBE: " << distanceCUBE << endl;
      }
      else
        distanceCUBE = 0;

      auto stop_timeCUBE = chrono::high_resolution_clock::now();

      // Distance True
      auto start_timeTrue = chrono::high_resolution_clock::now();
      double distanceTrue;
      if(metric == "euclidean"){
        int nn = D.NearestNeighbor(&(Q.Dspace[i]),metric);
        distanceTrue = euclidean_distance( &(D.Dspace[nn]) , &(Q.Dspace[i]) );
        cout << "distanceTrue: " << distanceTrue << "(" << nn << ")" << endl;
        out << "distanceTrue: " << distanceTrue  << "(" << nn << ")" << endl;
      }
      else if(metric == "cosine"){
        int nn = D.NearestNeighbor(&(Q.Dspace[i]),metric);
        distanceTrue = cosine_distance( &(D.Dspace[nn]) , &(Q.Dspace[i]) ) ;
        cout << "distanceTrue: " << distanceTrue << "(" << nn << ")" << endl;
        out << "distanceTrue: " << distanceTrue  << "(" << nn << ")" << endl;
      }
      auto stop_timeTrue = chrono::high_resolution_clock::now();

      // time CUBE
      double tCUBE = chrono::duration_cast<chrono::milliseconds>(stop_timeCUBE - start_timeCUBE).count();
      if( tCUBE == 0){
        cout << "tCUBE: ~1 milisecond" << endl;out << "tCUBE: ~1 milisecond" << endl;
        m_tCUBE += 1;
      }
      else{
        cout << "tCUBE: " << tCUBE << " miliseconds" << endl;out << "tCUBE: " << tCUBE << " miliseconds" << endl;
        m_tCUBE += tCUBE;
      }

      cout << "tTrue: " << chrono::duration_cast<chrono::milliseconds>(stop_timeTrue - start_timeTrue).count() << " miliseconds" << endl;
      out << "tTrue: " << chrono::duration_cast<chrono::milliseconds>(stop_timeTrue - start_timeTrue).count() << " miliseconds" << endl;

      // Calculate approach fraction
      appraoch_fraction = distanceCUBE/distanceTrue;
      if(appraoch_fraction > max_approach_fraction)
        max_approach_fraction = appraoch_fraction;
    }

    // finalize file
    cout << endl << max_approach_fraction << endl << m_tCUBE/queries << endl;
    out << endl << "Maximum Approach Fraction: " << max_approach_fraction << endl;
    out << "Mean time CUBE: " << m_tCUBE/queries << "miliseconds" << endl;

    // handle more query files
    cout << endl << "Continue for a next query file ? (yes/no)" << endl;
    cin >> answer;
    if(answer == "yes"){
      cout << "Enter the path of query file..." << endl;
      cin >> query_file;
      cout << "Enter radius..." << endl;
      cin >> Radius;
    }
    else
      break;
    out << "------------------------------------------------------------------" << endl;

  }while(1);
  auto stop_time = chrono::high_resolution_clock::now();

  out.close();
  cout << "HYPERCUBE Execution Time: " << chrono::duration_cast<chrono::seconds>(stop_time - start_time).count() << " seconds" << endl;
  cout << "HYPERCUBE Memory Usage: " << memory_usage << " bytes." << endl;
}
