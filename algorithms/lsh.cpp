#include <algorithm>
#include <ctime>
#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "../headers/HashTable.h"
#include "../headers/Insights.h"
#include "../headers/MathHelper.h"
#include "../headers/VectorSpace.h"

using namespace std;

int memory_usage; // stores the memory allocated for the basic data structures

//enum metrics{ euclidean, cosine }; // metrics available

int main(int argc, char *argv[]){
  memory_usage = 0; // start counting the memory
  auto start_time = chrono::high_resolution_clock::now();
  vector<string> Arguments(argv, argv + argc); // store all argument into a string vector

  // basic arguments
  string input_file, query_file, output_file, metric = "euclidean";
  int k=4, L=5;
  double appraoch_fraction, max_approach_fraction = -1, m_tLSH=0, Radius=0;

  // process all arguments
  for( int i = 0; i < argc; i++){
    if( Arguments[i] == "-d")
      input_file = Arguments[i+1];
    if( Arguments[i] == "-q")
      query_file = Arguments[i+1];
    if( Arguments[i] == "-o")
      output_file = Arguments[i+1];
    if( Arguments[i] == "-k")
      k = stoi(Arguments[i+1]);
    if( Arguments[i] == "-L")
      L = stoi(Arguments[i+1]);
    if( Arguments[i] == "-m")
      metric = (Arguments[i+1]);
    if( Arguments[i] == "-R")
      Radius = stod(Arguments[i+1]);
  }

  // output file
  ofstream out;
  out.open(output_file,ios::app);

  // program info on cout
  cout << endl << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ " << endl;
  cout << " VECTOR SPACE WILL BE BASED ON DATASET: " << input_file << endl;
  cout << " RUN THE QUERIES FROM: " << query_file << endl;
  cout << " THE RESULTS WILL BE STORED TO: " << output_file << endl;
  cout << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ " << endl;
  cout << " Algorithm used: LSH" << endl;
  cout << " Metric: " << metric << endl;
  cout << " #local-sensitive functions: " << k << "   #hash-tables: " << L << endl;
  cout << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ " << endl;

  // program info on output file
  time_t now = time(0);
  tm *ltm = localtime(&now);
  out << "------------------------------------------------------------------" << endl;
  out << "Algorithm: LSH " << "Metric: " << metric << " Radius: " << Radius << endl;
  out << "Using input file: " << input_file << endl;
  out << "#k: " << k << " #L: " << L << endl;
  out << ltm->tm_mday << "-" << 1 + ltm->tm_mon << "-" << 1900 + ltm->tm_year << " ";
  out << ltm->tm_hour << ":" << ltm->tm_min << ":" << 1 + ltm->tm_sec << endl;
  out << "------------------------------------------------------------------" << endl;

  cout << "Creating the vector space based on input file..." << endl;
  VectorSpace D(input_file); // create a vector space

  cout << "Creating L hash tables..." << endl;
  vector<int> r; // r family same for each hash table
  for(int i=0; i<k ; i++)
    r.push_back((rand() % 10)); // create the r family

  vector <HashTable> LTables ; // will store hash tables
  for(int i=0; i<L; i++){
    cout << "#" << i << " " ;
    HashTable Li(k,r,D.getDimension()); // create a hash table
    LTables.push_back(Li); // push it to the vector with the hash tables
    LTables[i].HashVectorSpace(&D,k,metric); // fill each hash table after hashing a vector space
    //LTables[i].printHashTable();
    //int temp;cin >> temp;
  }

  string answer; // for multiple query files
  do{
    cout << "Processing the query file" << endl;
    VectorSpace Q(query_file); // Will hold all query points
    out << endl << "Running queries from :" << query_file << "..." << endl << endl;

    // for every query
    int queries = Q.getNumberOfPoints();
    vector<int>* idsQ = Q.getIds();
    vector<int>* idsD = D.getIds();

    for(int i=0; i< queries ; i++){
      cout << endl << "Query: " << (*idsQ)[i] << endl; out << endl << "Query: " << (*idsQ)[i] << endl;

      double distanceLSH = 1000000;
      auto start_timeLSH = chrono::high_resolution_clock::now();

      // R-nearest neighbors calculation
      if(Radius > 0){ // Only approx nearest neighbor will be calculated
        out << "R-Near neighbors: " << endl;cout << "R-Near neighbors: " << endl;
      }
      vector<int> Result,results;

      // for each table
      for(int l=0 ; l<L ; l++){
        results = (LTables)[l].RangeSearch(Radius,&D,&(Q.Dspace[i]),k,D.getNumberOfPoints(),metric); // get nearest neighbors
        for(uint j=0 ; j < results.size() ; j++){
          Result.push_back(results[j]); // get all neighbors
        }
      }

      int numberNearestNeighbors = Result.size();
      if( numberNearestNeighbors == 0){
        if( Radius != 0){ // No point priting those if Radius is 0
          out << "None" << endl;cout << "None" << endl;
        }
      }
      else{
        // remove neighbors that are in list many times
        sort( Result.begin(), Result.end() );
        Result.erase(unique( Result.begin(), Result.end() ), Result.end() );
        // print the nearest neighbors within Radius range
        if(Radius > 0){
          for(uint p1 = 0 ; p1 < Result.size() ; p1++ ){
            out << (*idsD)[Result[p1]] << endl;cout << (*idsD)[Result[p1]] << endl; // the id of each near neighbor
          }
        }
      }

      int b = -1;
      // Approximate nearest neighbor
      if( numberNearestNeighbors > 0){
        out << "Nearest Neighbor: ";cout << "Nearest Neighbor: ";
        for(uint p1=0; p1<Result.size(); p1++){ // find the nearest neighbor from the neighbors returned
          if (metric == "euclidean"){
            double dist = euclidean_distance( &(D.Dspace[Result[p1]]) , &(Q.Dspace[i]) ); // calculate distance
            if( dist < distanceLSH){
              distanceLSH = dist;
              b = Result[p1];
            }
          }
          else if(metric == "cosine"){
            double dist = cosine_distance( &(D.Dspace[Result[p1]]) , &(Q.Dspace[i]) );
            if( dist < distanceLSH){
              distanceLSH = dist;
              b = Result[p1];
            }
          }
        }
        out << (*idsD)[b] << endl;cout << (*idsD)[b] << endl; // print the id of the nearest neighbor
        out << "distanceLSH: " << distanceLSH << endl;cout << "distanceLSH: " << distanceLSH << endl;
      }
      else
        distanceLSH = 0;

      auto stop_timeLSH = chrono::high_resolution_clock::now();

      // Distance True
      auto start_timeTrue = chrono::high_resolution_clock::now();
      double distanceTrue;
      if(metric == "euclidean"){
        int nn = D.NearestNeighbor(&(Q.Dspace[i]),metric); // returns the nearest neighbor
        distanceTrue = euclidean_distance( &(D.Dspace[nn]) , &(Q.Dspace[i]) ) ;
        out << "distanceTrue: " << distanceTrue << "(" << nn << ")" << endl;
        cout << "distanceTrue: " << distanceTrue << "(" << nn << ")" << endl;
      }
      else if(metric == "cosine"){
        int nn = D.NearestNeighbor(&(Q.Dspace[i]),metric);
        distanceTrue = cosine_distance( &(D.Dspace[nn]) , &(Q.Dspace[i]) );
        out << "distanceTrue: " << distanceTrue << "(" << nn << ")" << endl;
        cout << "distanceTrue: " << distanceTrue << "(" << nn << ")" << endl;
      }
      auto stop_timeTrue = chrono::high_resolution_clock::now();

      // time LSH
      double tLSH = chrono::duration_cast<chrono::milliseconds>(stop_timeLSH - start_timeLSH).count();
      if( tLSH == 0){
        out << "tLSH: ~1 milisecond" << endl; cout << "tLSH: ~1 milisecond" << endl;
        m_tLSH += 1;
      }
      else{
        out << "tLSH: " << tLSH << " miliseconds" << endl;cout << "tLSH: " << tLSH << " miliseconds" << endl;
        m_tLSH += tLSH;
      }

      out << "tTrue: " << chrono::duration_cast<chrono::milliseconds>(stop_timeTrue - start_timeTrue).count() << " miliseconds" << endl;
      cout << "tTrue: " << chrono::duration_cast<chrono::milliseconds>(stop_timeTrue - start_timeTrue).count() << " miliseconds" << endl;

      // Calculate approach fraction
      appraoch_fraction = distanceLSH/distanceTrue;
      if(appraoch_fraction > max_approach_fraction)
        max_approach_fraction = appraoch_fraction;

    }

    // finalize file
    cout << endl << max_approach_fraction << endl << m_tLSH/Q.getNumberOfPoints() << endl;
    out << endl << "Maximum Approach Fraction: " << max_approach_fraction << endl;
    out << "Mean time LSH: " << m_tLSH/Q.getNumberOfPoints() << "miliseconds" << endl;

    // handle more query files
    cout << endl << "Continue for a next query file ? (yes/no)" << endl;
    cin >> answer;
    if(answer == "yes"){
      cout << "Enter the path of query file..." << endl;
      cin >> query_file;
      cout << "Enter radius..." << endl;
      cin >> Radius;
    }
    else
      break;
    out << "------------------------------------------------------------------" << endl;

  }
  while(1);

  auto stop_time = chrono::high_resolution_clock::now();

  out.close();
  cout << "LSH Execution Time: " << chrono::duration_cast<chrono::seconds>(stop_time - start_time).count() << " seconds" << endl;
  cout << "LSH Memory Usage: " << memory_usage << " bytes." << endl;
}
