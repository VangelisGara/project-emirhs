CC=g++

SRC := sources
OBJ := objects

SOURCES := $(wildcard $(SRC)/*.cpp)
OBJECTS := $(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(SOURCES))

lsh: $(OBJECTS)
	$(CC) -g -Wall algorithms/lsh.cpp $^ -o $@

cube: $(OBJECTS)
	$(CC) -g -Wall algorithms/cube.cpp $^ -o $@

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) -g -I$(SRC) -c $< -o $@

clean:
	@rm -f D ./objects/*.o core
	@rm -f lsh
	@rm -f cube
